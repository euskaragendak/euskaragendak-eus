---
title: Aurkezpena
description: Tokian tokiko euskarazko ekitaldiak
date: 2022-09-07
author: Txopi
menu: main
featured:
  url: /img/avatar.png # relative path of the image
---

## Tokian tokiko euskarazko ekitaldiak ##

* Antzezlan bat euskaraz ikusteko gogoa duzu?
* Euskal musika talde baten kontzertura joan nahi duzu lagunekin?
* Haurrentzako ipuin kontalaririk edo pailazorik egongo den jakin nahi duzu?
* Berbalagun talde batera joan nahi duzu euskara praktikatzera?

Era honetako galderak erantzuten laguntzea da euskaragendak ekimenaren helburua. Horretarako, tokian tokiko eragileei eta norbanakoei agenda partekatu bat elikatzea proposatzen dizuegu, zuei eta euskaldun orori euskarazko ekitaldiak aurkitzen laguntzeko eta parte hartzera animatzeko. Euskaraz bizitzeko, euskaragendak laguntza eta akuilu izan daiteke!

Eskura dituzun agendak:

* ![irudia](/img/euskaragendak-logo-urdina-20px.png "irudia") **[Oarsoaldea](https://oarsoaldea.euskaragendak.eus/)**. Oarsoaldeko euskarazko ekintzen agenda. Herritik eta herriarentzat.
* ![irudia](/img/euskaragendak-logo-granatea-20px.png "irudia") **[Hegolapurdi](https://hegolapurdi.euskaragendak.eus/)**. Agenda partekatua tokiko eragileentzat.
* ![irudia](/img/euskaragendak-berria-20px.png "irudia") Zure agenda hemen gehi dezakezu.

## Nola parte hartu ##

Sartu agenda batean, **sakatu "+"** ikurra ekitaldi bat sortzeko eta bete formularioa. Ekitaldiak kartela edo irudia badu, "Media" sakatu eta aukeratu ezazu. Azkenik, **sakatu "BIDALI"** botoia eta kito!

Agendan **izena ematea** gomendatzen dizugu. Era horretan zuk bidalitakoa atoan argitatatuko da (administratzaileek zure erabiltzailea onartutakoan). Gainera, zuk bidalitako ekitaldiak zuzendu eta osatu ahal izango dituzu behar izanez gero. Ekitaldiak anonimoki bidaltzen badituzu pazientzia pixka bat izan behar duzu administratzaileren batek zuk bidalitakoa ikusi arte eta onartu arte.

## Partaideak ##

Euskaragendak ekimena Barakaldoko [Sasiburu](https://www.sasiburu.eus/) euskara taldeak sortua da.

Agenda bakoitza tokian tokiko talde, erakunde eta norbanakoek elikatzen dute.

Zuk parte hartzen duzun talde, elkarte edo erakundearen ekitaldiak dagokion euskaragendan ikusgai izatea nahi baduzu, zure laguntza ezinbestekoa da: onena, zuk zeuk webgunean izena ematea eta zuzenean informazioa aldiro-aldiro argitaratzea da. **Oso erraza eta azkarra da!**

Euskaragenda berri bat abian jarri nahi baduzu edo bestelako zalantzarik edo proposamenik baduzu, jar zaitez gurekin harremanetan: [info@sasiburu.eus](mailto:info@sasiburu.eus).

## Laguntzailea ##

Ondoko webguneak [Sindominio](https://sindominio.net/) proiektuak sortu ditu eta martxan jarrai dezaten arduratzen da, besteak beste, Gancio softwarea eguneratuz: [euskaragendak.eus](https://euskaragendak.eus/), [oarsoaldea.euskaragendak.eus](https://oarsoaldea.euskaragendak.eus/), [hegolapurdi.euskaragendak.eus](https://hegolapurdi.euskaragendak.eus/).

## Zer da Gancio ##

Gancio hitza *gantxo* ahoskatzen da eta lagunarteko italieraz hitzordua esan nahi du. Gancio, Italiako software libreko garatzaile batzuek tokian tokiko komunitateetan agenda partekatuak kudeatzeko sortu duten aplikazio baten izena ere bada. Bere ezaugarri nagusiak ondokoak dira:
* Software librea da.
* Euskaraz dago.
* Oso erabilerraza da.
* Fokua edukietan jartzen du, ez pertsonetan.
* Bisitariek dute lehentasuna.
* Ekitaldiak inportatzeko eta esportatzeko aukera desberdinak eskaintzen ditu.

Informazio gehiago: [gancio.org](https://gancio.org/)
